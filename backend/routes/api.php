<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\RoomController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::prefix('auth')
    ->controller(AuthController::class)
    ->group(function ()
    {
        Route::get('login', 'login');
        Route::get('guest', 'loginAsGuest');
    });

Route::middleware('auth:sanctum')->group(function ()
{
    Route::get('auth/logout', [AuthController::class, 'logout']);

    Route::prefix('users')
        ->controller(UserController::class)
        ->group(function ()
        {
            Route::get('', 'list');
            Route::get('profile', 'profile');
            Route::get('create', 'create');
            Route::get('update/{user}', 'update');
            Route::get('delete/{user}', 'update');
        });

    Route::prefix('rooms')
        ->controller(RoomController::class)
        ->group(function ()
        {
            Route::get('user/{user}', 'list');
            Route::get('create', 'create');
            Route::get('{room}/join', 'join');
            Route::get('{room}/leave', 'leave');
            Route::get('{room}/delete', 'delete');
        });
});
