<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\User\UserService;
use App\Services\User\Dto\CreateUserDto;
use App\Models\Enums\UserRole;

class ProvideDaemonsCommand extends Command
{
    protected $signature = 'daemons:provide';
    protected $description = 'Provide daemons';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        exec('ln -s '.app_path().'/Console/Daemons/websocket/websocket.service /etc/systemd/system/websocket.service');

        $this->info('Services provided');

        return 0;
    }
}
