<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\Websocket\WebsocketService;

class WebsocketCommand extends Command
{
    protected $signature = 'websocket:start-server';
    protected $description = 'Start websocket server';


    public function __construct(private readonly WebsocketService $websocketService)
    {
        parent::__construct();
    }

    public function handle()
    {
        $this->websocketService->startServer();
        $this->info('Webscoket server is started');

        return 0;
    }
}
