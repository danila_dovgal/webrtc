<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\User\UserService;
use App\Services\User\Dto\CreateUserDto;
use App\Models\Enums\UserRole;

class CreateAdminCommand extends Command
{
    protected $signature = 'users:create-admin';
    protected $description = 'Create admin';

    public function __construct(
        private readonly UserService $userService
    )
    {
        parent::__construct();
    }

    public function handle()
    {
        $this->userService->createUser(new CreateUserDto(
            phone: '+79167434679',
            email: 'danildovgal@gmail.com',
            password: 'admin',
            name: 'Admin',
            role: UserRole::ADMIN,
        ));
        $this->info('Admin created');

        return 0;
    }
}
