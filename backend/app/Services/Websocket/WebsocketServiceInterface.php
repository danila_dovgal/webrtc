<?php

namespace App\Services\Websocket;

interface WebsocketServiceInterface {
    function startServer(): void;
}