<?php

namespace App\Services\Websocket;

use Ratchet\Server\IoServer;
use Ratchet\Http\HttpServer;
use Ratchet\WebSocket\WsServer;
use App\Services\Chat\ChatService;

class WebsocketService implements WebsocketServiceInterface
{
    private $server;

    public function __construct(
        private readonly ChatService $chatService
    )
    {
    }

    public function startServer(): void
    {
        $this->server = IoServer::factory(
            new HttpServer(
                new WsServer(
                    $this->chatService
                )
            ),
            8080
        );

        $this->server->run();
    }
}
