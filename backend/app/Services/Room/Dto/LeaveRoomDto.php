<?php

namespace App\Services\Room\Dto;

class LeaveRoomDto
{
    public function __construct(
        public readonly int $roomId,
        public readonly int $userId,
        public readonly string $websocketId,
    )
    {
    }
}
