<?php

namespace App\Services\Room\Dto;

class CreateRoomDto
{
    public function __construct(
        public readonly int $userId
    )
    {
    }
}
