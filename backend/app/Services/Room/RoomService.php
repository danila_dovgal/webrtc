<?php

namespace App\Services\Room;

use Illuminate\Support\Str;
use App\Services\Room\Dto\CreateRoomDto;
use App\Services\Room\Dto\JoinRoomDto;
use App\Services\Room\Dto\LeaveRoomDto;
use App\Models\Room;
use App\Models\RoomUser;

class RoomService
{

    public function __construct()
    {
    }

    public function createRoom(CreateRoomDto $dto): ?Room
    {
        $room = Room::create([
            'user_id' => $dto->userId,
            'link' => uniqid() . Str::random(10),
        ]);

        return $room;
    }

    public function deleteRoom($room): void
    {
        $room->delete();
    }

    public function joinRoom(JoinRoomDto $dto): void
    {
        RoomUser::updateOrCreate([
            'room_id' => $dto->roomId,
            'user_id' => $dto->userId,
            'websocket_id' => $dto->websocketId,
        ]);
    }

    public function leaveRoom(LeaveRoomDto $dto): void
    {
        RoomUser::where([
            'room_id' => $dto->roomId,
            'user_id' => $dto->userId,
            'websocket_id' => $dto->websocketId,
        ])->delete();
    }
}
