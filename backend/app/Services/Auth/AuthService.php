<?php

namespace App\Services\Auth;

use Illuminate\Support\Facades\Hash;
use Illuminate\Auth\AuthenticationException;
use App\Services\Auth\Dto\LoginDto;
use App\Services\Auth\Dto\LoginAsGuestDto;
use App\Services\User\UserService;
use App\Services\User\Dto\CreateUserDto;
use App\Models\Enums\UserRole;
use App\Models\User;

class AuthService
{

    public function __construct(
        private readonly UserService $userService
    )
    {
    }

    public function login(LoginDto $dto): string
    {
        $user = User::where('email', $dto->login)->orWhere('phone', $dto->login)->first();

        if ($user && Hash::check($dto->password, $user->password))
        {
            return $user->createToken('access_token')->plainTextToken;
        }

        throw new AuthenticationException('Invalid credentials');
    }

    public function loginAsGuest(LoginAsGuestDto $dto): string
    {
        $user = $this->userService->createUser(
            new CreateUserDto(
                phone: null,
                email: null,
                password: null,
                name: $dto->name,
                role: UserRole::GUEST,
            )
        );

        return $user->createToken('access_token')->plainTextToken;
    }

    public function logout(User $user): void
    {
        $personalAccessToken = $user->currentAccessToken();
        $personalAccessToken->delete();
        $user->save();
        if ($user->isGuest())
        {
            $user->userRoom->delete();
            $user->delete();
        }
    }
}
