<?php

namespace App\Services\Auth\Dto;

class LoginDto
{
    public function __construct(
        public readonly string $login,
        public readonly string $password,
    )
    {
    }
}
