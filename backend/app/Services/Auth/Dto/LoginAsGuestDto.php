<?php

namespace App\Services\Auth\Dto;

class LoginAsGuestDto
{
    public function __construct(
        public readonly string $name,
    )
    {
    }
}
