<?php

namespace App\Services\User;

use App\Services\User\Dto\CreateUserDto;
use App\Services\User\Dto\UpdateUserDto;
use App\Models\User;

class UserService
{

    public function __construct()
    {
    }

    public function createUser(CreateUserDto $dto): ?User
    {
        $user = User::create([
            'phone' => $dto->phone,
            'email' => $dto->email,
            'password' => $dto->password,
            'name' => $dto->name,
            'role' => $dto->role,
        ]);

        return $user;
    }

    public function updateUser(User $user, UpdateUserDto $dto): void
    {
        $user->update([
            'phone' => $dto->phone,
            'email' => $dto->email,
            'password' => $dto->password,
            'name' => $dto->name,
            'role' => $dto->role,
        ]);
    }

    public function deleteUser(User $user): void
    {
        $user->delete();
    }
}
