<?php

namespace App\Services\User\Dto;

use App\Models\Enums\UserRole;

class UpdateUserDto
{
    public function __construct(
        public readonly ?string $phone = null,
        public readonly ?string $email = null,
        public readonly ?string $password = null,
        public readonly ?string $name = null,
        public readonly ?UserRole $role = null,
    )
    {
    }
}
