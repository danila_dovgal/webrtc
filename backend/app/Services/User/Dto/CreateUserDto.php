<?php

namespace App\Services\User\Dto;

use App\Models\Enums\UserRole;

class CreateUserDto
{
    public function __construct(
        public readonly ?string $phone = null,
        public readonly ?string $email = null,
        public readonly ?string $password = null,
        public readonly string $name,
        public readonly UserRole $role,
    )
    {
    }
}
