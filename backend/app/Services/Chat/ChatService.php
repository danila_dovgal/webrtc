<?php

namespace App\Services\Chat;

use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;
use \SplObjectStorage;
use Laravel\Sanctum\PersonalAccessToken;
use App\Services\Room\RoomService;
use App\Services\Room\Dto\JoinRoomDto;
use App\Services\Room\Dto\LeaveRoomDto;
use App\Models\User;
use App\Models\Room;
use App\Models\RoomUser;

class ChatService implements MessageComponentInterface
{
    protected $clients;
    protected $userToRoom;

    public function __construct(
        private readonly RoomService $roomService
    )
    {
        $this->clients = new SplObjectStorage;
        $this->userToRoom = [];
    }

    public function onOpen(ConnectionInterface $conn)
    {
        $queryString = $conn->httpRequest->getUri()->getQuery();
        parse_str($queryString, $query);

        if (!$token = PersonalAccessToken::findToken($query['token']))
        {
            return false;
        }

        if (!$user = $token->tokenable)
        {
            return false;
        }

        if (!$room = Room::link($query['link'])->first())
        {
            return false;
        }

        if ($room->roomUser->count() >= 2)
        {
            return false;
        }

        $this->userToRoom[$user->id] = $room->id;
        $this->roomService->joinRoom(
            new JoinRoomDto(
                userId: $user->id,
                roomId: $room->id,
                websocketId: $conn->resourceId,
            )
        );

        $this->clients->attach($conn);

        $this->sendMessage($conn, $room, ['type' => 'user-connected']);
    }

    public function onMessage(ConnectionInterface $conn, $msg)
    {
        $data  = json_decode($msg, true);
        $room  = Room::where('link', $data['link'])->first();

        $this->sendMessage($conn, $room, $data);
    }

    public function onClose(ConnectionInterface $conn)
    {
        $this->logout($conn);
        $this->clients->detach($conn);
    }

    public function onError(ConnectionInterface $conn, \Exception $e)
    {
        $this->logout($conn);
        $conn->close();
    }

    private function sendMessage(ConnectionInterface $conn, Room $room, array $data): void
    {
        foreach ($this->clients as $client)
        {
            if ($conn !== $client)
            {
                foreach ($room->roomUser as $roomUser)
                {
                    if ($client->resourceId == $roomUser->websocket_id)
                    {
                        $client->send(json_encode($data));
                    }
                }
            }
        }
    }

    private function logout(ConnectionInterface $conn): bool
    {
        if (!$roomUser = RoomUser::where('websocket_id', $conn->resourceId)->first())
        {
            return true;
        }

        if (!$user = $roomUser->user)
        {
            return true;
        }

        $room = $roomUser->room;

        $this->roomService->leaveRoom(
            new LeaveRoomDto(
                userId: $user->id,
                roomId: $this->userToRoom[$user->id],
                websocketId: $conn->resourceId,
            )
        );

        unset($this->userToRoom[$user->id]);

        $this->sendMessage($conn, $room, ['type' => 'user-disconnected']);

        return true;
    }
}
