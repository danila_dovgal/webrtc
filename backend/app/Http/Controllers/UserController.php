<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use App\Services\User\UserService;
use App\Services\User\Dto\CreateUserDto;
use App\Services\User\Dto\UpdateUserDto;
use App\Models\User;
use App\Models\Enums\UserRole;

class UserController extends Controller
{
    public function __construct(
        private readonly UserService $userService
    )
    {
    }

    public function list(Request $request): JsonResponse
    {
        $users = User::all();

        return response()->json(
            [
                'users' => $users,
            ]
        );
    }

    public function create(Request $request): JsonResponse
    {
        $user = $this->userService->createUser(
            new CreateUserDto(
                phone: $request->input('phone'),
                email: $request->input('email'),
                password: $request->input('password'),
                name: $request->input('name'),
                role: UserRole::USER,
            )
        );
        return response()->json(
            [
                'user' => $user,
            ]
        );
    }

    public function update(Request $request, User $user): Response
    {
        $this->userService->updateUser(
            $user,
            new UpdateUserDto(
                phone: $request->input('phone'),
                email: $request->input('email'),
                password: $request->input('password'),
                name: $request->input('name'),
            )
        );
        return response()->noContent();
    }

    public function delete(Request $request, User $user): Response
    {
        if ($this->getAuthUser() != $user)
        {
            $this->userService->deleteUser($user);
        }
        return response()->noContent();
    }

    public function profile(Request $request): JsonResponse
    {
        $user = $this->getAuthUser();
        return response()->json(
            [
                'user' => $user,
            ]
        );
    }
}
