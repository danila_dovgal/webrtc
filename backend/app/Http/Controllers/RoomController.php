<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use App\Services\Room\RoomService;
use App\Services\Room\Dto\CreateRoomDto;
use App\Services\Room\Dto\JoinRoomDto;
use App\Services\Room\Dto\LeaveRoomDto;
use App\Models\User;
use App\Models\Room;

class RoomController extends Controller
{
    public function __construct(
        private readonly RoomService $roomService
    )
    {
    }

    public function list(Request $request, User $user): JsonResponse
    {
        return response()->json(
            [
                'rooms' => $user->rooms,
            ]
        );
    }

    public function create(Request $request): JsonResponse
    {
        $user = $this->getAuthUser();
        $room = $this->roomService->createRoom(
            new CreateRoomDto(
                userId: $user->id
            )
        );
        return response()->json(
            [
                'room' => $room,
            ]
        );
    }

    public function delete(Request $request, Room $room): Response
    {
        $this->roomService->deleteRoom($room);
        return response()->noContent();
    }

    public function join(Request $request, Room $room): Response
    {
        $user = $this->getAuthUser();
        $this->roomService->joinRoom(
            new JoinRoomDto(
                roomId: $room->id,
                userId: $user->id,
            )
        );
        return response()->noContent();
    }

    public function leave(Request $request, Room $room): Response
    {
        $user = $this->getAuthUser();
        $this->roomService->leaveRoom(
            new LeaveRoomDto(
                roomId: $room->id,
                userId: $user->id,
            )
        );
        return response()->noContent();
    }
}
