<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use App\Services\Auth\AuthService;
use App\Services\Auth\Dto\LoginDto;
use App\Services\Auth\Dto\LoginAsGuestDto;

class AuthController extends Controller
{
    public function __construct(
        private readonly AuthService $authService,
    )
    {
    }

    public function login(Request $request): JsonResponse
    {
        $token = $this->authService->login(
            new LoginDto(
                login: $request->input('login'),
                password: $request->input('password')
            )
        );
        return response()->json(
            [
                'token' => $token,
            ]
        );
    }

    public function loginAsGuest(Request $request): JsonResponse
    {
        $token = $this->authService->loginAsGuest(
            new LoginAsGuestDto(
                name: $request->input('name')
            )
        );
        return response()->json(
            [
                'token' => $token,
            ]
        );
    }

    public function logout(): Response
    {
        $user = $this->getAuthUser();
        $this->authService->logout($user);
        return response()->noContent();
    }
}
