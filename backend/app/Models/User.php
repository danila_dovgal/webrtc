<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use App\Models\Enums\UserRole;


class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    protected $fillable = [
        'name',
        'phone',
        'email',
        'password',
        'role',
    ];

    protected $hidden = [
        'password',
    ];

    protected $casts = [
        'password' => 'hashed',
        'role' => UserRole::class,
    ];

    public function isUser(): bool
    {
        return $this->role == UserRole::USER;
    }

    public function isGuest(): bool
    {
        return $this->role == UserRole::GUEST;
    }

    public function rooms(): HasMany
    {
        return $this->hasMany(Room::class);
    }

    public function roomUser(): HasMany
    {
        return $this->hasMany(RoomUser::class);
    }
}
