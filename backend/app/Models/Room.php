<?php

namespace App\Models;

use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Room extends Model
{
    protected $fillable = [
        'user_id',
        'link',
    ];

    protected $hidden = [];

    protected $casts = [];

    public function roomUser(): HasMany
    {
        return $this->hasMany(RoomUser::class);
    }

    public function scopeLink(Builder $query, string $link): Builder
    {
        return $query->where('link', $link);
    }
}
