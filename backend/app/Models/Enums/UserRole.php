<?php

namespace App\Models\Enums;

enum UserRole: string
{
    case USER = 'user';
    case GUEST = 'guest';
    case ADMIN = 'admin';
}
