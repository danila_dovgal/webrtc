import './assets/css/index.css';
import './assets/css/main.css';

import { createApp, ref } from 'vue';
import axios from 'axios';
import App from './App.vue';
import { createRouter, createWebHistory } from 'vue-router';
import LoginView from '@/views/LoginView.vue';

const token = ref(localStorage.getItem("token"));

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/:link?',
      name: 'login',
      props: true,
      component: LoginView
    },
    {
      path: '/home',
      name: 'home',
      props: true,
      component: () => import('@/views/HomeView.vue')
    },
    {
      path: '/room/:link?/:token?',
      name: 'room',
      props: true,
      component: () => import('@/views/RoomView.vue')
    }
  ]
});

router.beforeEach(async (to, from) => {
  if (!token.value) {
    if (to.name == 'login') {
      return true;
    } else if (to.name == 'room' && to.params.link) {
      if (to.params.token) {
        return true;
      } else {
        return { name: 'login', params: { link: to.params.link } };
      }
    } else {
      return { name: 'login' };
    }
  } else {
    if (to.name == 'home' || to.name == 'room') {
      return true;
    } else {
      return { name: 'home' }
    }
  }
});

axios.defaults.headers.common["Authorization"] = `Bearer ${token.value}`;

const app = createApp(App);

app.provide("token", token);
app.use(router);
app.mount('#app');
