import { inject } from "vue";
import axios from "axios";

export function useRequest() {
  const token = inject("token");

  if (token) {
    axios.defaults.headers.common["Authorization"] = `Bearer ${token.value}`;
  }

  const parseJSON = (response = {}) => {
    if (response.status === 204 || response.status === 205) {
      return null;
    }
    return response.data;
  };

  const checkStatus = ({ response = {} }) => {
    if (response.status >= 200 && response.status < 300) {
      return response;
    }

    throw new Error(response.statusText);
  };

  const request = async (url, options = {}) => {
    return axios(import.meta.env.VITE_API_URL + url, options)
      .then(parseJSON)
      .catch(checkStatus);
  };

  return request;
}
